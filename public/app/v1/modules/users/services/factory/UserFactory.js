angular.module('user')

.factory('UserFactory', ['BASE', '$http', function (BASE, $http) {

	var url = BASE.API_VERSION + 'user';
	var factory = {};

	factory.all = function(){
		return $http.get(url);
	}

	factory.authenticate = function(params){
		return $http.post(url + '/authenticate', params);
	}

	factory.view = function(id){
		return $http.get(url + '/view', {id:id});
	}

	factory.edit = function(id){
		return $http.get(url + '/edit', {id:id});
	}

	factory.update = function(params){
		return $http.post(url + '/update', params);
	}

	factory.destroy = function(id){
		return $http.get(url + '/destory', {id:id});
	}

	factory.store = function(params){
		return $http.post(url + '/store', params);
	}
	
	return factory;
}])
