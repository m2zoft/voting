angular.module('user')

.controller('userController', ['$scope', '$state', 'BASE', '$http', 'UserFactory', '$cookies', function($scope, $state, BASE, $http, UserFactory, $cookies){

	$scope.login = function(){

		UserFactory.authenticate($scope.user).then(function(response){

			var data = response.data;

			var expireDate = new Date();
  			expireDate.setDate(expireDate.getDate() + 1);

			$cookies.put('token', data.token, {expires:expireDate});
			$cookies.put('username', data.username, {expires:expireDate});
			$cookies.put('role', data.role, {expires:expireDate});
			console.log(data.token);
			
			swal({
			  title: "System Message",
			  text: 'Credentials has been authenticated. You will redirect to your dashboard.',
			  type: 'success',
			}, function(){
				$state.go('admin.index');
			});
		},
		function(){
			swal({
			  title: "System Message",
			  text: 'Credentials not valid.',
			  type: 'error',
			});
		});
	}

}])


.controller('dashboardController', ['$scope', 'resolvedData', 'VotingFactory', function ($scope, resolvedData, VotingFactory) {

	$scope.data = resolvedData;

	if($scope.data.voting_status == 0){ $scope.data.voting_status = false; }else{ $scope.data.voting_status = true; }
	if($scope.data.has_voting_login == 0){ $scope.data.has_voting_login = false; }else{ $scope.data.has_voting_login = true; }
	if($scope.data.has_registration == 0){ $scope.data.has_registration = false; }else{ $scope.data.has_registration = true; }
	if($scope.data.show_result == 0){ $scope.data.show_result = false; }else{ $scope.data.show_result = true; }

	$scope.switch = {
		voting_status: $scope.data.voting_status,
		has_voting_login: $scope.data.has_voting_login,
		has_registration: $scope.data.has_registration,
		show_result: $scope.data.show_result,
	};

	$scope.voting_status = $scope.switch.voting_status;
	$scope.show_result = $scope.switch.show_result;

	$scope.switchFunction = {
		voting: function(){
			$scope.voting_status = $scope.switch.voting_status;
			VotingFactory.updateVotingStatus($scope.switch).then(function(){
				setTimeout(function(){
					swal({
					  title: "System Message",
					  text: 'Voting status has been successfully changed.',
					  type: 'success',
					});
				},500);
			});
		},
		voting_registration: function(){
			VotingFactory.updateVotingRegistration($scope.switch).then(function(){
				setTimeout(function(){
					swal({
					  title: "System Message",
					  text: 'Voting Registration status has been successfully changed.',
					  type: 'success',
					});
				},500);
			});
		},
		voting_login: function(){
			VotingFactory.updateVotingLogin($scope.switch).then(function(){
				setTimeout(function(){
					swal({
					  title: "System Message",
					  text: 'Voting Login status has been successfully changed.',
					  type: 'success',
					});
				},500);
			});
		},
		voting_result: function(){
			$scope.show_result = $scope.switch.show_result;
			VotingFactory.updateVotingResult($scope.switch).then(function(){
				setTimeout(function(){
					swal({
					  title: "System Message",
					  text: 'Voting Result status has been successfully changed.',
					  type: 'success',
					});
				},500);
			});
		}
	}

}])

.controller('voterRegistrationController', ['$scope', '$state', '$cookies', function ($scope, $state, $cookies) {
	
	$scope.registerVoter = function(){
		alert($cookies.get('token'));
		swal({
		  title: "System Message",
		  text: 'New Voter has been registered.',
		  type: 'success',
		  timer: '1000',
		}, function(){
			$state.go('admin.voters');
		});
	}

}])


.controller('candidatesController', ['switchResolve', '$scope', 'positionService', 'PositionFactory', '$state', 'votingTour', function(switchResolve, $scope, positionService, PositionFactory, $state, votingTour){

	$scope.startTour = function(){
		votingTour.start();
	}


	$scope.voting_status = switchResolve[0].voting;
	$scope.is_open = function() {
		switch($scope.voting_status){
			case 0:
			return false;

			case 1:
			return true;
		}
	};

	$scope.is_started = function(){
		if(switchResolve[0].started_at == '0000-00-00 00:00:00'){
			return false;
		}else{
			return true;
		}
	}


	$scope.positions = [];

	$scope.$watch($scope.positions);

	$scope.getPositions = function(){
		PositionFactory.all().then(function(response){
			$scope.positions = response.data;	
		});
	}

	setTimeout(function(){ $scope.getPositions(); }, 100);

	$scope.addNewPositionForm = function(){
		positionService.addPosition($scope.positions);
	}

	$scope.editPosition = function(position_obj){
		positionService.editPosition(position_obj);
	}

	$scope.deletePosition = function(position_id){
		positionService.deletePosition($scope.positions, position_id);
	}

	$scope.assignCandidate = function(position_obj, position_id){
		positionService.assignCandidate($scope.positions, position_obj, position_id);
	}

	$scope.removeAssignedCandidate = function(position_obj, candidate_obj){
		positionService.removeAssignedCandidate(position_obj, candidate_obj);

	}


	$scope.viewListOfCandidates = function(){
		$state.go('admin.candidates-list');
	}


}])


.controller('candidatesListController', ['$scope', 'positionService', 'personResolve', function ($scope, positionService, personResolve) {

	$scope.persons = personResolve;

	$scope.addNewCandidate = function(){
		positionService.addNewCandidate($scope.persons);
	}


}])


.controller('ReportsController', ['$scope', 'resolvedData', 'ReportsFactory', function ($scope, resolvedData, ReportsFactory) {

	$scope.data = resolvedData;

	$scope.reports = [];

	if($scope.data.voting_status == 0){ $scope.data.voting_status = false; }else{ $scope.data.voting_status = true; }
	if($scope.data.has_voting_login == 0){ $scope.data.has_voting_login = false; }else{ $scope.data.has_voting_login = true; }
	if($scope.data.has_registration == 0){ $scope.data.has_registration = false; }else{ $scope.data.has_registration = true; }
	if($scope.data.show_result == 0){ $scope.data.show_result = false; }else{ $scope.data.show_result = true; }

	$scope.switch = {
		voting_status: $scope.data.voting_status,
		has_voting_login: $scope.data.has_voting_login,
		has_registration: $scope.data.has_registration,
		show_result: $scope.data.show_result,
	};

	$scope.show_result = $scope.switch.show_result;
	
	$scope.generateReport = function(){
		
		ReportsFactory.generate($scope.report).then(function(response){
			$scope.reports = response.data;
		}, function(err){
			console.log(err);
		});

	}



}])

