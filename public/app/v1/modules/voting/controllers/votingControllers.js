angular.module('voting')

.controller('tourController', ['votingTour','$scope',function(votingTour, $scope){

	$scope.startTour = function(){
		votingTour.start();
	}

	
}])

.controller('votingController', ['switchResolve', '$filter', '$state','$scope', 'votingService', '$filter', 'votingWizard', '$cookies', 'VoterFactory', function(switchResolve, $filter, $state, $scope, votingService, $filter, votingWizard, $cookies, VoterFactory){

	$scope.voting_status = switchResolve[0].voting;
	$scope.is_open = function() {
		switch($scope.voting_status){
			case 0:
			return false;

			case 1:
			return true;
		}

	};

	$scope.voter = {
		voter_code: $cookies.get('voter_code'),
		positions: []
	};

	votingService.getPositionAndCandidates().then(function(response){
		$scope.voting = response.data;

		for (var i = 0; i < $scope.voting.length; i++) {
			var positions = {
				id: $scope.voting[i].position.id,
				title: $scope.voting[i].position.title,
				selected_candidates: [],
			};
			$scope.voter.positions.push(positions);
		}


		setTimeout(function(){
			votingWizard.start();
			$(document).find('.submit-vote').click(function() {
				$scope.submitVote();
			});
		},10);
	});


	$scope.selectCandidate = function(event, vote_obj, position_obj, candidate_obj){

		if ($(event.currentTarget).find('.profile').hasClass('voting-selected')) {

			var obj = $filter('filter')($scope.voter.positions, {id:position_obj.id});

			for (var i = 0; i < obj[0].selected_candidates.length; i++) {
				if (obj[0].selected_candidates[i].id == candidate_obj.id) {
					delete obj[0].selected_candidates[i];
					obj[0].selected_candidates.splice(i, 1);
				}
			}

			vote_obj.position.elected_count += 1;
			$(event.currentTarget).find('.profile').removeClass('voting-selected');
			$(event.currentTarget).find('.btn.btn-primary').removeClass('btn-primary').addClass('btn-success').html('<span class="glyphicon glyphicon-thumbs-up"></span> Vote');
		}else{
			if (vote_obj.position.elected_count >= 1) {

				vote_obj.position.elected_count -= 1;
				var obj = $filter('filter')($scope.voter.positions, {id:position_obj.id});
				obj[0].selected_candidates.push(candidate_obj);

				$(event.currentTarget).find('.profile').addClass('voting-selected');
				$(event.currentTarget).find('.btn.btn-success').removeClass('btn-success').addClass('btn-primary').html('<span class="fa fa-check"></span> Selected');

			}else{
				swal({
				  title: "System Message",
				  text: 'You have reached the maximum candidate(s) to be selected.',
				});
			}
		}

	}


	$scope.submitVote = function(){
		swal({
		  title: "System Message",
		  text: 'Are you sure you want to submit your vote?',
		  showCancelButton: true,
		  confirmButtonColor: "#DD6B55",
		  confirmButtonText: "Yes",
		  cancelButtonText: "Cancel",
		  closeOnConfirm: false,
		  closeOnCancel: false,
		  html: true,
		  showLoaderOnConfirm: true
		},
		function(isConfirm){
		  if (isConfirm) {
		  	VoterFactory.submitVote($scope.voter).then(function(success){
		  		swal({
			    	title: "Submitted!",
				  	text: "Your vote has been submitted. Thank you.",
				  	type: 'success',
				  	timer: 2000,
			    }, function(){
			    	$state.go('voter-login');
			    });
			    
		  	}, function(err){
		  		var error_message = err.data;
                var element = '<ul class="notification">';
                $.each(error_message, function(index, val) {
                    element += '<li>' + val[0] + '</li>';
                });
                element += '</ul>';

                swal({
                  title: "System Message",
                  text: element,
                  type: 'error',
                  html: true,
                });
		  	});


		    
		  } else {
			swal({
		    	title: "Cancelled!",
			  	text: "Submission of your vote has been cancelled, you can now review your vote and submit again.",
			  	type: 'error',
			  	timer: 2000,
			  	showConfirmButton: false
		    });
		  }
		});
	};


}])


