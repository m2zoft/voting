angular.module('voting')

.service('votingService', ['BASE', '$http', function(BASE, $http) {

	var url = BASE.API_VERSION + 'voting';
	this.getPositionAndCandidates = function(){
		return $http.get(url);
	}

}])