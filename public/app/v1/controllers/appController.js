angular.module('app')

.controller('voterLoginController', ['$scope', '$state', 'switchResolve', '$cookies', 'SwitchFactory', function($scope,$state,switchResolve,$cookies,SwitchFactory){
	
	$scope.login_status = switchResolve[0].has_voting_login;
	$scope.has_login = function() {
		switch($scope.login_status){
			case 0:
			return false;

			case 1:
			return true;
		}

	};

	$scope.proceed = function(){

		switch($scope.login_status){
			case 0:
				SwitchFactory.generateVoterCode().then(function(response){
					$cookies.remove('voter_code');
					$cookies.put('voter_code', response.data);

					$state.go('voting.index');
				});
				break;

			case 1:
				return true;
				break;
		}

	}

}])