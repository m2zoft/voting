angular.module('app')

.factory('authInterceptor', ['$location', '$q', '$window', '$cookies', '$injector', 'BASE', function($location, $q, $window, $cookies, $injector, BASE) {
	return {
	    request: function(config) {

	      	config.headers = config.headers || {};

	      	config.headers.Authorization = 'Bearer ' + $cookies.get('token');

	      	return config;
	    },

	    responseError: function(config){

	    	if (config.status == 401 || config.status == 400) {
	    		$injector.get('$state').go('login');
	    	}

	    	return $q.reject(config);

	    }
  	};
}])


.factory('PositionFactory', ['BASE', '$http', function (BASE, $http) {

	var url = BASE.API_VERSION + 'position';
	var factory = {};

	factory.all = function(){
		return $http.get(url);
	}

	factory.view = function(id){
		return $http.get(url + '/view', {id:id});
	}

	factory.edit = function(id){
		return $http.get(url + '/edit', {id:id});
	}

	factory.update = function(params){
		return $http.post(url + '/update', params);
	}

	factory.destroy = function(id){
		return $http.get(url + '/destroy', {params: {id:id}});
	}

	factory.destroyAssignedCandidate = function(params){
		return $http.post(url + '/destroy-assigned-candidate', params);
	}

	factory.store = function(params){
		return $http.post(url + '/store', params);
	}
	
	return factory;
}])



.factory('CandidateFactory', ['BASE', '$http', function (BASE, $http) {

	var url = BASE.API_VERSION + 'candidate';
	var factory = {};

	factory.all = function(){
		return $http.get(url);
	}

	factory.getCandidates = function (){
		// Candidates
		return $http.get(url + 's');
	}

	factory.view = function(id){
		return $http.get(url + '/view', {id:id});
	}

	factory.edit = function(id){
		return $http.get(url + '/edit', {id:id});
	}

	factory.update = function(params){
		return $http.post(url + '/update', params);
	}

	factory.destroy = function(id){
		return $http.get(url + '/destroy', {params: {id:id}});
	}

	factory.store = function(params){
		return $http.post(url + '/store', params);
	}
	
	return factory;
}])


.factory('PersonFactory', ['BASE', '$http', function (BASE, $http) {

	var url = BASE.API_VERSION + 'person';
	var factory = {};

	factory.all = function(){
		return $http.get(url);
	}

	factory.allWithPosition = function(){
		return $http.get(url + '/with-position');
	}

	factory.getAvailable = function(){
		return $http.post(url + '/available');
	}

	factory.addToCandidate = function(params){
		return $http.post(url + '/add-to-candidate', params);
	}

	factory.view = function(id){
		return $http.get(url + '/view', {id:id});
	}

	factory.edit = function(id){
		return $http.get(url + '/edit', {id:id});
	}

	factory.update = function(params){
		return $http.post(url + '/update', params);
	}

	factory.destroy = function(id){
		return $http.get(url + '/destroy', {params: {id:id}});
	}

	factory.store = function(params){
		return $http.post(url + '/store', params);
	}
	
	return factory;
}])


.factory('VoterFactory', ['BASE', '$http', function (BASE, $http) {

	var url = BASE.API_VERSION + 'voter';
	var factory = {};

	factory.all = function(){
		return $http.get(url);
	}

	factory.view = function(id){
		return $http.get(url + '/view', {id:id});
	}

	factory.submitVote = function(params){
		return $http.post(url + '/submit-vote', params)
	}


	factory.update = function(params){
		return $http.post(url + '/update', params);
	}

	factory.destroy = function(id){
		return $http.get(url + '/destroy', {params: {id:id}});
	}

	factory.store = function(params){
		return $http.post(url + '/store', params);
	}
	
	return factory;
}])


.factory('VotingFactory', ['BASE', '$http', function (BASE, $http) {

	var url = BASE.API_VERSION + 'voting';
	var factory = {};

	factory.all = function(){
		return $http.get(url);
	}

	factory.updateVotingStatus = function(params){
		return $http.post(url + '/update-voting-status', params);
	}

	factory.updateVotingRegistration = function(params){
		return $http.post(url + '/update-voting-registration', params);
	}

	factory.updateVotingLogin = function(params){
		return $http.post(url + '/update-voting-login', params);
	}

	factory.updateVotingResult = function(params){
		return $http.post(url + '/update-voting-result', params);
	}

	factory.view = function(id){
		return $http.get(url + '/view', {id:id});
	}

	factory.update = function(params){
		return $http.post(url + '/update', params);
	}

	factory.destroy = function(id){
		return $http.get(url + '/destroy', {params: {id:id}});
	}

	factory.store = function(params){
		return $http.post(url + '/store', params);
	}
	
	return factory;
}])


.factory('SwitchFactory', ['BASE', '$http', function (BASE, $http) {
	
	var url = BASE.API_VERSION + 'switch';
	var factory = {};

	factory.get = function(){
		return $http.get(url);
	}

	factory.generateVoterCode = function(){
		return $http.get(url + '/generate-voter-code');
	}

	
	return factory;

}])


.factory('ReportsFactory', ['BASE', '$http', function (BASE, $http) {
	
	var url = BASE.API_VERSION + 'report';
	var factory = {};

	factory.generate = function(params){
		return $http.post(url + '/generate', params);
	}
	
	return factory;

}])