angular.module('app')


.service('pageLoadingFrameService', [function () {
    this.pageLoadingFrame = function(action,ver){    
    
        ver = ver ? ver : 'v2';
        
        var pl_frame = $("<div></div>").addClass("page-loading-frame");
        
        if(ver === 'v2')
            pl_frame.addClass("v2");
        
        var loader = new Array();
        loader['v1'] = '<div class="page-loading-loader"><img src="assets/img/loaders/page-loader.gif"/></div>';
        loader['v2'] = '<div class="page-loading-loader"><div class="dot1"></div><div class="dot2"></div></div>';
        
        if(action == "show" || !action){
            $("body").append(pl_frame.html(loader[ver]));
        }
        
        if(action == "hide"){
            
            if($(".page-loading-frame").length > 0){
                $(".page-loading-frame").addClass("removed");

                setTimeout(function(){
                    $(".page-loading-frame").remove();
                },800);        
            }
            
        }
    }
}])


.service('votingWizard', [function () {

    this.start = function(){
        $(".wizard").smartWizard({                        
            // This part of code can be removed FROM
            onLeaveStep: function(obj){
                var wizard = obj.parents(".wizard");

                if(wizard.hasClass("wizard-validation")){
                    
                    var valid = true;
                    
                    $('input,textarea',$(obj.attr("href"))).each(function(i,v){
                        valid = validator.element(v) && valid;
                    });
                                                
                    if(!valid){
                        wizard.find(".stepContainer").removeAttr("style");
                        validator.focusInvalid();                                
                        return false;
                    }         
                    
                }    
                
                return true;
            },// <-- TO
            
            //This is important part of wizard init
            onShowStep: function(obj){                        
                var wizard = obj.parents(".wizard");

                if(wizard.hasClass("show-submit")){
                
                    var step_num = obj.attr('rel');
                    var step_max = obj.parents(".anchor").find("li").length;

                    if(step_num == step_max){                             
                        obj.parents(".wizard").find(".actionBar .btn-primary").css("display","block");
                    }                         
                }
                return true;                         
            }//End
        });

        $(".actionBar").css('padding-bottom', '20px');
    }

}])

// Voting Tour
.service('votingTour', [function(){

    this.start = function(){
        return introJs().start();
    }
    
}])

// Add Candidate
.service('positionService', ['$uibModal', 'BASE', 'PositionFactory', '$filter', '$state', function ($uibModal, BASE, PositionFactory, $filter, $state) {

    // positions is an object
    this.addPosition = function(positions){
        $uibModal.open({
            templateUrl: BASE.API_URL + 'modules/voting/templates/position-form.html',
            size: 'md',
            controller: function($scope, $uibModalInstance, $http, PositionFactory){

                $scope.cancelModal = function(){
                    $uibModalInstance.dismiss('cancel');
                }

                $scope.add = function(){
                    PositionFactory.store($scope.position).then(function(success){

                        positions.push(success.data[0]);
                        console.log(positions);
                        swal({
                          title: "System Message",
                          text: 'New Position has been added.',
                          type: 'success',
                        }, function(){
                            $uibModalInstance.dismiss('cancel');
                        });
                        

                    }, function(err){
                        var error_message = err.data;
                        var element = '<ul class="notification">';
                        $.each(error_message, function(index, val) {
                            element += '<li>' + val[0] + '</li>';
                        });
                        element += '</ul>';

                        swal({
                          title: "System Message",
                          text: element,
                          type: 'error',
                          html: true,
                        });
                    })
                    
                }

            }
        });
    }

    this.editPosition = function(position){
        $uibModal.open({
            templateUrl: BASE.API_URL + 'modules/voting/templates/edit-position-form.html',
            size: 'md',
            controller: function($scope, $uibModalInstance, $http, PositionFactory){

                $scope.position = position;

                $scope.cancelModal = function(){
                    $uibModalInstance.dismiss('cancel');
                }

                $scope.save = function(){
                    PositionFactory.update($scope.position).then(function(success){

                        swal({
                          title: "System Message",
                          text: 'Old Position has been changed into '+$scope.position.title,
                          type: 'success',
                        }, function(){
                            $uibModalInstance.dismiss('cancel');
                        });
                        

                    }, function(err){
                        var error_message = err;
                        var element = '<ul class="notification">';
                        $.each(error_message, function(index, val) {
                            element += '<li>' + val[0] + '</li>';
                        });
                        element += '</ul>';


                        swal({
                          title: "System Message",
                          text: element,
                          type: 'error',
                          html: true,
                        });
                    })
                    
                }

            }
        });
    }

    this.deletePosition = function(positions_obj, position_id){
        swal({
          title: "Are you sure?",
          text: "You will not be able to recover this position and its candidates!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, delete it!",
          cancelButtonText: "No!",
          closeOnConfirm: false,
          closeOnCancel: false
        },
        function(isConfirm){
          if (isConfirm) {
            PositionFactory.destroy(position_id).then(function(success){
                
                for (var i = 0; i < positions_obj.length; i++) {
                    if (positions_obj[i].id == position_id) {
                        delete positions_obj[i];
                        positions_obj.splice(i,1);
                    }
                }

                new_positions_obj = $filter('filter')(positions_obj, {id:!position_id});
                positions_obj = new_positions_obj;
                // positions_obj.splice($filter('filter')(positions_obj, {id:success.data}), 1);

                console.log(positions_obj);
                
                swal("Deleted!", "This position and its candidates has been deleted.", "success");
            }, function(){
                swal("Error!", "There is something wrong while deleting this positions and its candidates", "error");
            });
            
          } else {
            swal("Cancelled", "This position is safe :)", "error");
          }
        });
    }

    this.assignCandidate = function(positions_obj, position_obj, position_id){
        $uibModal.open({
            templateUrl: BASE.API_URL + 'modules/voting/templates/candidates.html',
            size: 'md',
            controller: function($scope, $uibModalInstance, $http, PersonFactory, $filter){

                $scope.title = position_obj.title;

                PersonFactory.getAvailable().then(function(response){
                    $scope.candidates = response.data;
                });

                $scope.selected_candidates = [];

                $scope.cancelModal = function(){
                    $uibModalInstance.dismiss('cancel');
                }

                $scope.assign = function(candidate_obj){
                    candidate_obj.selected = true;
                    candidate_obj.position_id = position_id;
                    $scope.selected_candidates.push(candidate_obj);
                }

                $scope.unAssign = function(candidate_obj){
                    candidate_obj.selected = false;
                    for (var i = 0; i < $scope.selected_candidates.length; i++) {
                        if ($scope.selected_candidates[i].id == candidate_obj.id) {
                            delete $scope.selected_candidates[i];
                            $scope.selected_candidates.splice(i, 1);
                        }
                    }
                }

                $scope.add = function(){

                    PersonFactory.addToCandidate($scope.selected_candidates).then(function(response){
                        swal({
                          title: "System Message",
                          text: 'New candidate/s has been added',
                          type: 'success',
                        }, function(){
                            $state.reload();
                            $uibModalInstance.dismiss('cancel');
                        });


                    }, function(err){
                        swal({
                          title: "System Message",
                          text: 'There is something wrong while adding candidate/s.',
                          type: 'error',
                        }, function(){
                            $uibModalInstance.dismiss('cancel');
                        });
                    });
                }


            }
        });
    }

    this.removeAssignedCandidate = function(position_obj, candidate_obj){
        swal({
          title: "Are you sure?",
          text: "You will remove this person from the list of candidates!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, remove it!",
          cancelButtonText: "No!",
          closeOnConfirm: false,
          closeOnCancel: false
        },
        function(isConfirm){
          if (isConfirm) {
            PositionFactory.destroyAssignedCandidate(candidate_obj).then(function(success){

                for (var i = 0; i < position_obj.candidates.length; i++) {
                    if (position_obj.candidates[i].id == candidate_obj.id) {
                        delete position_obj.candidates[i];
                        position_obj.candidates.splice(i,1);
                    }
                }

                swal("Deleted!", "Person has been removed from the list of candidates", "success");
            }, function(){
                swal("Error!", "There is something wrong while removal of this candidate.", "error");
            });
            
          } else {
            swal("Cancelled", "The removal of this candidate has been cancelled.", "error");
          }
        });
    }   

    this.addNewCandidate = function(persons_obj){
        $uibModal.open({
            templateUrl: BASE.API_URL + 'modules/voting/templates/candidate-form.html',
            size: 'md',
            controller: function($scope, $uibModalInstance, $http, PersonFactory){

                $scope.cancelModal = function(){
                    $uibModalInstance.dismiss('cancel');
                }

                $scope.add = function(){

                    PersonFactory.store($scope.candidate).then(function(success){

                        persons_obj.push(success.data[0]);

                        swal({
                          title: "System Message",
                          text: 'New candidate has been added.',
                          type: 'success',
                        }, function(){
                            $uibModalInstance.dismiss('cancel');
                        });
                        

                    }, function(error){

                        var error_message = error.data;
                        var element = '<ul class="notification">';
                                $.each(error_message, function(index, val) {
                                    element += '<li>' + val[0] + '</li>';
                                });
                            element += '</ul>';


                        swal({
                          title: "System Message",
                          text: element,
                          type: 'error',
                          html:true,
                        });
                    })
                    
                }

            }
        });
    }

}])