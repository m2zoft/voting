angular.module('app')
.config(['$stateProvider', '$urlRouterProvider', 'BASE', '$qProvider', '$httpProvider', function($stateProvider, $urlRouterProvider, BASE, $qProvider, $httpProvider) {

    $urlRouterProvider.otherwise('/');
    $qProvider.errorOnUnhandledRejections(false);
    
    $httpProvider.interceptors.push('authInterceptor');

    $stateProvider

        // USER LOGIN
        .state('login', {
            url: '/',
            views: {
                'app': {
                    templateUrl: BASE.API_URL + BASE.TEMPLATE_URL + 'users/templates/user-login.html',
                    controller: 'userController',
                }
            }
        })

        // DASHBOARD ADMIN
        .state('admin', {
            url: '/admin',
            abstract: true,
            views: {
                'app': {
                    templateUrl: BASE.API_URL + BASE.TEMPLATE_URL + 'users/templates/admin.html',
                }
            },
            resolve: {
                role: function($q, $cookies){
                    if ($cookies.get('role') !== 'Administrator') {
                        return $q.reject('Unauthorized');
                    }
                }
            }
        })

        .state('admin.index', {
            url: '/index',
            views: {
                'content': {
                    templateUrl: BASE.API_URL + BASE.TEMPLATE_URL + 'users/templates/admin.index.html',
                    controller: 'dashboardController',
                }
            },
            resolve: {
                resolvedData: function(VoterFactory){
                    return VoterFactory.all().then(function(response){ return response.data; });
                }
            }
        })

        .state('admin.voters', {
            url: '/voters',
            views: {
                'content': {
                    templateUrl: BASE.API_URL + BASE.TEMPLATE_URL + 'users/templates/voter.lists.html',
                }
            }
        })

        .state('admin.add-voter', {
            url: '/add',
            views: {
                'content': {
                    templateUrl: BASE.API_URL + BASE.TEMPLATE_URL + 'users/templates/add-voter.html',
                    controller: 'voterRegistrationController',
                }
            }
        })

        .state('admin.candidates', {
            url: '/candidates',
            views: {
                'content': {
                    templateUrl: BASE.API_URL + BASE.TEMPLATE_URL + 'users/templates/candidates.html',
                    controller: 'candidatesController',
                }
            },
            resolve: {
                switchResolve: function(SwitchFactory){
                    return SwitchFactory.get().then(function(response){ return response.data; });
                }
            }
        })

        .state('admin.candidates-list', {
            url: '/candidates/list',
            views: {
                'content': {
                    templateUrl: BASE.API_URL + BASE.TEMPLATE_URL + 'voting/templates/list-of-candidates.html',
                    controller: 'candidatesListController',
                },
            },
            resolve: {
                personResolve: function(PersonFactory){
                    return PersonFactory.allWithPosition().then(function(response){ return response.data; });
                }
            }
        })


        .state('admin.reports', {
            url: '/reports',
            views: {
                'content': {
                    templateUrl: BASE.API_URL + BASE.TEMPLATE_URL + 'users/templates/reports.html',
                    controller: 'ReportsController',
                },
            },
            resolve: {
                resolvedData: function(VoterFactory){
                    return VoterFactory.all().then(function(response){ return response.data; });
                }
            }
        })

        

        


        // DASHBOARD FOR VOTING
        .state('voter-login', {
            url: '/start',
            views: {
                'app': {
                    templateUrl: BASE.API_URL + BASE.TEMPLATE_URL + 'voting/templates/index.html',
                    controller: 'voterLoginController',
                }
            },
            resolve: {
                switchResolve: function(SwitchFactory){
                    return SwitchFactory.get().then(function(response){ return response.data; });
                }
            }
        })

        .state('voting', {
            url: '/vote',
            abstract: true,
            views: {
                'app': {
                    templateUrl: BASE.API_URL + BASE.TEMPLATE_URL + 'voting/templates/vote.html',
                    controller: 'tourController',
                }
            }
        })

        .state('voting.index', {
            url: '/index',
            views: {
                'content': {
                    templateUrl: BASE.API_URL + BASE.TEMPLATE_URL + 'voting/templates/vote.index.html',
                    controller: 'votingController',
                }
            },
            resolve: {
                switchResolve: function(SwitchFactory){
                    return SwitchFactory.get().then(function(response){ return response.data; });
                }
            }
        })


}])