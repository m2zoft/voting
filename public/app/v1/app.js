angular.module('app', ['ui.router', 'ngCookies', 'ngFileUpload', 'ui.bootstrap', 'voting', 'user'])
.constant('BASE', {
	'API_URL': 'http://voting.local/app/v1/',
	'SYSTEM_TITLE': 'BIZWEX VOTING SYSTEM',
	'SYSTEM_LOGO': 'VOTEWEX',
	'SYSTEM_VERSION': 'V.1.0',
	'TEMPLATE_URL': 'modules/',
	'ASSETS_URL': 'http://voting.local/assets/',
	'API_VERSION': 'api/v1/',
})

.run(['$rootScope', 'BASE', '$state', '$templateCache','pageLoadingFrameService', '$cookies', function ($rootScope, BASE, $state, $templateCache, pageLoadingFrameService, $cookies) {
	
	$rootScope.APP_TITLE = BASE.SYSTEM_TITLE;
	$rootScope.APP_LOGO = BASE.SYSTEM_LOGO;
	$rootScope.APP_VERSION = BASE.SYSTEM_VERSION;
	$rootScope.APP_USER = 'http://votewex.com/assets/img/user.png';
	$rootScope.APP_ASSETS_URL = BASE.ASSETS_URL;

	$rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams){
        pageLoadingFrameService.pageLoadingFrame("show");
    });

    $rootScope.$on("$stateChangeSuccess", function(event, toState, toParams, fromState, fromParams){
        pageLoadingFrameService.pageLoadingFrame("hide");
    });


    $rootScope.$on('$stateChangeError', function(e, toState, toParams, fromState, fromParams, error){

	    if(error === "Unauthorized"){
	        $state.go("login");
	    }

	});
	
    $.ajaxSetup({
        headers: {
            'Authorization': 'Bearer ' + $cookies.get('token')
        }
    });

}]);