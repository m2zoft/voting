<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/




// Voting
Route::get('voting', '\Bizwex\Voting\Controllers\VotingController@index');
Route::post('voting/update-voting-status', '\Bizwex\Voting\Controllers\VotingController@updateVotingStatus');
Route::post('voting/update-voting-registration', '\Bizwex\Voting\Controllers\VotingController@updateVotingRegistration');
Route::post('voting/update-voting-login', '\Bizwex\Voting\Controllers\VotingController@updateVotingLogin');
Route::post('voting/update-voting-result', '\Bizwex\Voting\Controllers\VotingController@updateVotingResult');

Route::post('user/authenticate', '\Bizwex\Voting\Controllers\UserController@authenticate');

// Switch/Settings
Route::get('switch', '\Bizwex\Voting\Controllers\SwitchController@index');
Route::get('switch/generate-voter-code', '\Bizwex\Voting\Controllers\VoterController@generateVoterCode');


// Position
Route::get('position', '\Bizwex\Voting\Controllers\PositionController@index');
Route::get('position/view', '\Bizwex\Voting\Controllers\PositionController@view');
Route::get('position/edit', '\Bizwex\Voting\Controllers\PositionController@edit');
Route::post('position/update', '\Bizwex\Voting\Controllers\PositionController@update');
Route::get('position/destroy', '\Bizwex\Voting\Controllers\PositionController@destroy');
Route::post('position/destroy-assigned-candidate', '\Bizwex\Voting\Controllers\PositionController@destroyAssignedCandidate');
Route::post('position/store', '\Bizwex\Voting\Controllers\PositionController@store');

// Candidate
Route::get('candidate', '\Bizwex\Voting\Controllers\CandidateController@index');
Route::get('candidate/view', '\Bizwex\Voting\Controllers\CandidateController@view');
Route::get('candidate/edit', '\Bizwex\Voting\Controllers\CandidateController@edit');
Route::post('candidate/update', '\Bizwex\Voting\Controllers\CandidateController@update');
Route::get('candidate/destroy', '\Bizwex\Voting\Controllers\CandidateController@destroy');
Route::post('candidate/store', '\Bizwex\Voting\Controllers\CandidateController@store');

// Person
Route::get('person', '\Bizwex\Voting\Controllers\PersonController@index');
Route::get('person/with-position', '\Bizwex\Voting\Controllers\PersonController@withPosition');
Route::post('person/available', '\Bizwex\Voting\Controllers\PersonController@available');
Route::post('person/add-to-candidate', '\Bizwex\Voting\Controllers\PersonController@addToCandidate');
Route::get('person/view', '\Bizwex\Voting\Controllers\PersonController@view');
Route::get('person/edit', '\Bizwex\Voting\Controllers\PersonController@edit');
Route::post('person/update', '\Bizwex\Voting\Controllers\PersonController@update');
Route::get('person/destroy', '\Bizwex\Voting\Controllers\PersonController@destroy');
Route::post('person/store', '\Bizwex\Voting\Controllers\PersonController@store');


// Voter
Route::get('voter', '\Bizwex\Voting\Controllers\VoterController@index');
Route::get('voter/view', '\Bizwex\Voting\Controllers\VoterController@view');
Route::get('voter/edit', '\Bizwex\Voting\Controllers\VoterController@edit');
Route::post('voter/update', '\Bizwex\Voting\Controllers\VoterController@update');
Route::get('voter/destroy', '\Bizwex\Voting\Controllers\VoterController@destroy');
Route::post('voter/store', '\Bizwex\Voting\Controllers\VoterController@store');

Route::post('voter/submit-vote', '\Bizwex\Voting\Controllers\VoterController@submitVote');



// Reports
Route::post('report/generate', '\Bizwex\Voting\Controllers\ReportController@generate');


Route::group(['middleware' => 'jwt.auth'], function() {

	Route::get('user/create', '\Bizwex\Voting\Controllers\UserController@store');
});



