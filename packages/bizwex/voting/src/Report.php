<?php 

namespace Bizwex\Voting;


use Bizwex\Voting\Handlers\PDFInterface;
use Bizwex\Voting\Handlers\EXCELInterface;
use Bizwex\Voting\Handlers\ViewingInterface;

class Report
{
	public function viewing(ViewingInterface $viewing, $params)
	{
		return $viewing->generate($params);
	}

	public function pdf(PDFInterface $pdf, $params)
	{
		return $pdf->generate($params);
	}

	public function excel(EXCELInterface $excel, $params)
	{
		return $excel->generate($params);
	}

}