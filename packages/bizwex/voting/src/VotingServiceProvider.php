<?php

namespace Bizwex\Voting;

use Illuminate\Support\ServiceProvider;

class VotingServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // include __DIR__.'/api.php';
        // $this->app->make('Bizwex\Inventory\Controllers');
    }
}
