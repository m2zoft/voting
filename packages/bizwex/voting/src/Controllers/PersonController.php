<?php

namespace Bizwex\Voting\Controllers;

use App\Http\Controllers\Controller;
use Request;
use Response;

use Bizwex\Voting\Models\PersonModel;

use Bizwex\Voting\Requests\PersonRequest;
use Bizwex\Voting\Models\CandidatesModel;

class PersonController extends Controller
{

    public function index()
    {
        $persons = PersonModel::all();

        return $persons;
    }


    public function withPosition()
    {
        $persons = PersonModel::all();
        if (count($persons) > 0) {
            foreach ($persons as $person) {

                $person['position_id'] = 0;
                $person['position_title'] = 'none';

                $candidate_found = CandidatesModel::leftJoin('positions as P', 'candidates.position_id', '=', 'P.id')->where('candidates.person_id', $person['id'])->select('candidates.*', 'P.title as position_title', 'P.id as position_id')->get();
                if (count($candidate_found) > 0) {
                    $person['position_id'] = $candidate_found[0]['position_id'];
                    $person['position_title'] = $candidate_found[0]['position_title'];
                }
            }
        }
        return $persons;
    }

    public function available()
    {


        $persons = PersonModel::all();
        $available_candidates = [];
        if (count($persons) > 0) {
            foreach ($persons as $person) {
                $candidate_found = CandidatesModel::where('person_id', $person['id'])->get();
                if (count($candidate_found) < 1) {
                    $person['selected'] = false;
                    array_push($available_candidates, $person);
                }
            }
        }

        return $available_candidates;
    }


    public function addToCandidate()
    {
        $request = Request::all();
        $candidates = [];
        if (count($request) > 0) {
            foreach ($request as $candidate) {
                CandidatesModel::create(['position_id' => $candidate['position_id'], 'person_id' => $candidate['id']]);
            }
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PersonRequest $request)
    {

        $last_id = PersonModel::create(['firstname' => $request['firstname'],'middlename' => $request['middlename'],'lastname' => $request['lastname'],'address' => $request['address'],'mobile_number' => $request['mobile_number'], 'photo' => 'no-image.jpg']);
        
        $persons = PersonModel::where('id', $last_id['id'])->get();
        if (count($persons) > 0) {
            foreach ($persons as $person) {

                $person['position_id'] = 0;
                $person['position_title'] = 'none';

                $candidate_found = CandidatesModel::leftJoin('positions as P', 'candidates.position_id', '=', 'P.id')->where('candidates.person_id', $person['id'])->select('candidates.*', 'P.title as position_title', 'P.id as position_id')->get();
                if (count($candidate_found) > 0) {
                    $person['position_id'] = $candidate_found[0]['position_id'];
                    $person['position_title'] = $candidate_found[0]['position_title'];
                }
            }
        } 

        return $persons;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PositionRequest $request)
    {
        $title = $request['title'];
        $elected_count = $request['elected_count'];
        $id = $request['id'];

        if (count(PositionModel::CheckTitle($title)->get()) > 1) {
            return Response()->json(['Position already exist. Try a new position.'], 422);
        }

        PositionModel::where('id', $id)->update([
            'title' => $title,
            'elected_count' => $elected_count,
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        $request = Request::all();
        $id = $request['id'];
        CandidatesModel::where('position_id', $id)->delete();
        PositionModel::where('id', $id)->delete();

        return $id;
    }
}
