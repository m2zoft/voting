<?php

namespace Bizwex\Voting\Controllers;

use App\Http\Controllers\Controller;
use Request;
use Response;

use Bizwex\Voting\Models\UserModel;
use Sentinel;
use Auth;
use JWTAuth;
use Tymon\JWTAuthExceptions\JWTException;
use JWTFactory;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function authenticate()
    {
        $request = Request::all();
        $credentials = ['username' => $request['username'], 'password' => $request['password']];
        $status = 'error';
        $token = '';

        if(!$token = JWTAuth::attempt($credentials)){
            return Response()->json(['status' => $status], 422);
        }


        // Find User Role
        // $user = JWTAuth::parseToken()->toUser();

        $status = 'success';
        $role = 'Administrator';
        $username = $request['username'];

        return Response()->json(['status' => $status, 'token' => $token, 'role' => $role, 'username' => $username], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        // $request = Request::all();
        $username = 'admin';
        $password = '123456';
        $credentials = ['username' => $username, 'password' => $password];

        $user = Sentinel::registerAndActivate($credentials);

        $role = Sentinel::findRoleBySlug('sysadmin');

        $role->users()->attach($user);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
