<?php

namespace Bizwex\Voting\Controllers;

use App\Http\Controllers\Controller;
use Request;
use Response;

use Bizwex\Voting\Models\CandidatesModel;
use Bizwex\Voting\Models\PositionModel;
use Bizwex\Voting\Models\PersonModel;
use Bizwex\Voting\Models\SwitchModel;

class VotingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $positions = PositionModel::all();
        $voting = [];
        $ctr = 0;
        foreach ($positions as $position) {
            $ctr++;
            $candidates = CandidatesModel::where('position_id', $position->id)->get();
            if (count($candidates) > 0) {
                foreach ($candidates as $candidate) {
                    $person_found = PersonModel::where('id', $candidate['person_id'])->get();
                    $person = count($person_found) > 0 ? $person_found[0] : '';
                    $candidate['person'] = $person;
                }
            }
            $voting[] = ['voting_id' => $ctr, 'position' => $position, 'candidates' => $candidates, 'count' => $position->elected_count];
            // array_push(, );
        }

        return Response()->json($voting, 200);
    }

    
    public function updateVotingStatus()
    {
        $request = Request::all();
        $now = date('Y-m-d h:i:s');
        $is_started = SwitchModel::where('id', 1)->get()[0];

        if ($is_started['started_at'] == '0000-00-00 00:00:00') {
            SwitchModel::where('id', 1)->update(['voting' => $request['voting_status'], 'started_at' => $now]);
        }else{
            SwitchModel::where('id', 1)->update(['voting' => $request['voting_status']]);
        }
        
    }

    public function updateVotingRegistration(){
        $request = Request::all();
        SwitchModel::where('id', 1)->update(['has_registration' => $request['has_registration']]);
    }

    public function updateVotingLogin(){
        $request = Request::all();
        SwitchModel::where('id', 1)->update(['has_voting_login' => $request['has_voting_login']]);
    }

    public function updateVotingResult(){
        $request = Request::all();
        SwitchModel::where('id', 1)->update(['show_result' => $request['show_result']]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
