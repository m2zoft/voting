<?php

namespace Bizwex\Voting\Controllers;

use App\Http\Controllers\Controller;
use Request;
use Response;

use Bizwex\Voting\Models\SwitchModel;

class SwitchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $voting = SwitchModel::all();
       return $voting;
    }

}
