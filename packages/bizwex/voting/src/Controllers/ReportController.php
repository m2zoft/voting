<?php

namespace Bizwex\Voting\Controllers;

use App\Http\Controllers\Controller;
use Request;
use Response;

use Bizwex\Voting\Models\CandidatesModel;
use Bizwex\Voting\Models\PositionModel;
use Bizwex\Voting\Models\PersonModel;
use Bizwex\Voting\Models\SwitchModel;

use Bizwex\Voting\Report;
use Bizwex\Voting\PDF;
use Bizwex\Voting\Excel;
use Bizwex\Voting\Viewing;

class ReportController extends Controller
{
    public function generate()
    {   
        $params = Request::all();
        return (new Report)->viewing(new Viewing, $params);
    }
}
