<?php

namespace Bizwex\Voting\Controllers;

use App\Http\Controllers\Controller;
use Request;
use Response;

use Bizwex\Voting\Models\PositionModel;
use Bizwex\Voting\Models\CandidatesModel;
use Bizwex\Voting\Models\PersonModel;

use Bizwex\Voting\Requests\PositionRequest;

class PositionController extends Controller
{

    public function index()
    {
        $positions = PositionModel::all();
        if (count($positions) > 0) {
            foreach ($positions as $position) {
                $candidates = CandidatesModel::where('position_id', $position->id)->get();
                if (count($candidates) > 0) {
                    foreach ($candidates as $candidate) {
                        $person_found = PersonModel::where('id', $candidate['person_id'])->get();
                        $person = count($person_found) > 0 ? $person_found[0] : '';
                        $candidate['person'] = $person;
                    }
                }
                
                $position['candidates'] = $candidates;
            }
        }

        return $positions;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PositionRequest $request)
    {

        $title = $request['title'];
        $elected_count = $request['elected_count'];

        if (count(PositionModel::CheckTitle($title)->get()) > 0) {
            return Response()->json(['Position already exist. Try a new position.'], 422);
        }

        $newly_position_id = PositionModel::create([
            'title' => $title,
            'elected_count' => $elected_count,
        ]);

        $positions = PositionModel::where('id', $newly_position_id->id)->get();
        if (count($positions) > 0) {
            foreach ($positions as $position) {
                $candidates = CandidatesModel::where('position_id', $position->id)->get();
                $position['candidates'] = $candidates;
            }
        }


        return $positions;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PositionRequest $request)
    {
        $title = $request['title'];
        $elected_count = $request['elected_count'];
        $id = $request['id'];

        if (count(PositionModel::CheckTitle($title)->get()) > 1) {
            return Response()->json(['Position already exist. Try a new position.'], 422);
        }

        PositionModel::where('id', $id)->update([
            'title' => $title,
            'elected_count' => $elected_count,
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        $request = Request::all();
        $id = $request['id'];
        CandidatesModel::where('position_id', $id)->delete();
        PositionModel::where('id', $id)->delete();

        return $id;
    }

    public function destroyAssignedCandidate()
    {
        $request = Request::all();
        $position_id = $request['position_id'];
        $person_id = $request['person_id'];

        CandidatesModel::where('position_id', $position_id)->where('person_id', $person_id)->delete();
    }
}
