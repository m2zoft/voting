<?php

namespace Bizwex\Voting\Controllers;

use App\Http\Controllers\Controller;
use Request;
use Response;

use Bizwex\Voting\Models\CandidatesModel;
use Bizwex\Voting\Models\PositionModel;
use Bizwex\Voting\Models\PersonModel;
use Bizwex\Voting\Models\VoterModel;
use Bizwex\Voting\Models\SwitchModel;
use Bizwex\Voting\Models\VotesModel;


class VoterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
       
    $positions = PositionModel::all();
    $candidates = CandidatesModel::all();
    $voting = SwitchModel::all();

    $voting_status = $voting[0]['voting'];
    $has_voting_login = $voting[0]['has_voting_login'];
    $has_registration = $voting[0]['has_registration'];
    $show_result = $voting[0]['show_result'];


    $voters = VoterModel::where('status', 'voting')->get();
    foreach ($voters as $voter) {
        $voter['voted_at'] = date('M d, Y h:i:s', strtotime($voter['created_at']));
    }

    $current_voters = VoterModel::all();

    if (count($positions) > 0) {
        foreach ($positions as $position) {
            $candidate_info = CandidatesModel::leftJoin('persons as P', 'candidates.person_id', '=', 'P.id')->where('position_id', $position['id'])->select('candidates.*', 'P.firstname as firstname', 'P.middlename as middlename', 'P.lastname as lastname', 'P.address as address', 'P.mobile_number as mobile_number', 'P.photo as photo')->get();

            // Computations for the percentage result
            $base = 0;

            foreach ($candidate_info as $info) {
                $total_votes = VotesModel::where('person_id', $info['person_id'])->where('position_id', $position['id'])->get();
                $info['total_votes'] = count($total_votes);
                $base += count($total_votes);
            }

            foreach ($candidate_info as $info) {
                $val = $info['total_votes'];
                $info['percentage'] = ($val === 0) ? $val : floor(($val/$base) * 100);
            }

            $position['candidates'] = $candidate_info;
        }
    }

    // [
        // { y: '2006', a: 100, b: 90 },
        // { y: '2007', a: 75,  b: 65 },
        // { y: '2008', a: 50,  b: 40 },
        // { y: '2009', a: 75,  b: 65 },
        // { y: '2010', a: 50,  b: 40 },
        // { y: '2011', a: 75,  b: 65 },
        // { y: '2012', a: 100, b: 90 }
    // ]

    return ['voters' => $voters, 'total_voters' => count($current_voters), 'positions' => $positions, 'total_positions' => count($positions), 'candidates' => $candidates, 'total_candidates' => count($candidates), 'voting_status' => $voting_status, 'has_voting_login' => $has_voting_login, 'has_registration' => $has_registration, 'show_result' => $show_result];
 
    }

    public function generateVoterCode($voter = false)
    {
        if ($voter) {
            
            $code = strtoupper(substr($voter['firstname'], 0, 1) . substr($voter['middlename'], 0, 1) . substr($voter['lastname'], 0, 1));

            $voter_code = strtoupper(substr(md5($code), 0, 6));

            return $voter_code;            
        }else{

            $voter_code = strtoupper(substr(md5(date('ymdhis')), 0, 6));

            VoterModel::create(['voter_code' => $voter_code, 'status' => 'voting']);

            return $voter_code;
        }

    }

    public function submitVote()
    {
        $request = Request::all();

        $voter_code = $request['voter_code'];
        $positions = $request['positions'];

        foreach ($positions as $position) {
            foreach ($position['selected_candidates'] as $selected_candidate) {
                VotesModel::create([
                        'voter_code' => $voter_code,
                        'person_id' => $selected_candidate['person_id'],
                        'position_id' => $selected_candidate['position_id'],
                    ]);
            }
        }

        VoterModel::where('voter_code', $voter_code)->update(['status' => 'voted']);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
