<?php

namespace Bizwex\Voting\Models;

use Illuminate\Database\Eloquent\Model;

class PositionModel extends Model
{
    protected $table = 'positions';
    protected $fillable = ['title', 'elected_count'];

    public function scopeCheckTitle($query, $title)
    {
    	return $query->where('title', 'LIKE', $title.'%');
    }
}
