<?php

namespace Bizwex\Voting\Models;

use Illuminate\Database\Eloquent\Model;

class VotesModel extends Model
{
    protected $table = 'votes';
    protected $fillable = ['voter_code', 'person_id', 'position_id'];
}
