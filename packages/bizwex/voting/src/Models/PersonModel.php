<?php

namespace Bizwex\Voting\Models;

use Illuminate\Database\Eloquent\Model;

class PersonModel extends Model
{
    protected $table = 'persons';
    protected $fillable = ['firstname', 'middlename', 'lastname', 'address', 'mobile_number', 'photo'];
    
}
