<?php

namespace Bizwex\Voting\Models;

use Illuminate\Database\Eloquent\Model;

class SwitchModel extends Model
{
    protected $table = 'switch';
    protected $fillable = ['voting', 'started_at', 'ended_at', 'has_registration', 'has_voting_login', 'show_result'];


    public function scopeSwitch($query)
    {
    	return $query->all();
    }
}
