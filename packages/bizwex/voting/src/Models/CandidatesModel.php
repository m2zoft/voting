<?php

namespace Bizwex\Voting\Models;

use Illuminate\Database\Eloquent\Model;

class CandidatesModel extends Model
{
    protected $table = 'candidates';
    protected $fillable = ['position_id', 'person_id', 'total_votes', 'status'];
}
