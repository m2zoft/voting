<?php

namespace Bizwex\Voting\Models;

use Illuminate\Database\Eloquent\Model;

class VoterModel extends Model
{
    protected $table = 'voters';
    protected $fillable = ['voter_code', 'firstname', 'middlename', 'lastname', 'status'];
}
