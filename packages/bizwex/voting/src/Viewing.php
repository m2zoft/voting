<?php 

namespace Bizwex\Voting;

use Response;

use Bizwex\Voting\Models\VoterModel;
use Bizwex\Voting\Models\PositionModel;
use Bizwex\Voting\Models\CandidatesModel;
use Bizwex\Voting\Models\VotesModel;

use Bizwex\Voting\Handlers\ViewingInterface;

/**
* PDF Format
*/
class Viewing implements ViewingInterface
{

	private $type;
	private $order_by;
	private $order;

	public function generate($params)
	{
		if ($params) {
			$this->type = $params['type'];
			$this->order_by = isset($params['order_by']) ? $params['order_by'] : 'id';
			$this->order = isset($params['order']) ? $params['order'] : 'ASC';
			return $this->type();
		}else{
			return [];
		}
		
	}

	private function type(){
		switch ($this->type) {
			case 1:
				return $this->VotersList();
				break;
			case 2:
				return $this->VotersWhoVoted();
				break;
			case 3:
				return $this->VotersWhoNotVoted();
				break;
			case 4:
				return $this->ListOfPositionsAndCandidates();
				break;
			case 5:
				return $this->Results();
				break;
			default:
				return $this->VotersList();
				break;
		}
	}

	private function VotersList(){

		$order_by = $this->order_by;
		$order = $this->order;

		$voters = VoterModel::orderBy($order_by, $order)->get();
		foreach ($voters as $voter) {
			$voter['formatted_date'] = date('M d, Y h:i:s', strtotime($voter['created_at']));
		}
		$total_voted = VoterModel::where('status', 'voted')->orderBy($order_by, $order)->get();
		$total_not_voted = VoterModel::where('status', 'pending')->orderBy($order_by, $order)->get();

		return Response()->json(['type' => $this->type, 'data' => ['voters' => $voters, 'total' => count($voters), 'total_voted' => count($total_voted), 'total_not_voted' => count($total_not_voted)]]);
	}


	private function VotersWhoVoted(){

		$order_by = $this->order_by;
		$order = $this->order;

		$voters = VoterModel::where('status', 'voted')->orderBy($order_by, $order)->get();
		foreach ($voters as $voter) {
			$voter['formatted_date'] = date('M d, Y h:i:s', strtotime($voter['created_at']));
		}
		$total_voted = VoterModel::where('status', 'voted')->orderBy($order_by, $order)->get();

		return Response()->json(['type' => $this->type, 'data' => ['voters' => $voters, 'total' => count($voters), 'total_voted' => count($total_voted)]]);

	}

	private function VotersWhoNotVoted(){

		$order_by = $this->order_by;
		$order = $this->order;

		$voters = VoterModel::where('status', 'pending')->orderBy($order_by, $order)->get();
		foreach ($voters as $voter) {
			$voter['formatted_date'] = date('M d, Y h:i:s', strtotime($voter['created_at']));
		}
		$total_voted = VoterModel::where('status', 'pending')->orderBy($order_by, $order)->get();

		return Response()->json(['type' => $this->type, 'data' => ['voters' => $voters, 'total' => count($voters), 'total_voted' => count($total_voted)]]);

	}

	private function ListOfPositionsAndCandidates(){

		$order_by = $this->order_by;
		$order = $this->order;

		$positions = PositionModel::orderBy($order_by, $order)->get();

		if (count($positions) > 0) {
			foreach ($positions as $position) {
				$candidates = CandidatesModel::leftJoin('persons as P', 'candidates.person_id', '=', 'P.id')->select('candidates.*', 'P.*')->where('position_id', $position['id'])->get();
				$position['candidates'] = $candidates;
				$position['total_candidates'] = count($candidates);
				$position['formatted_date'] = date('M d, Y h:i:s', strtotime($position['created_at']));
			}
		}

		return Response()->json(['type' => $this->type, 'data' => ['positions' => $positions]]);

	}

	private function Results(){

		$order_by = $this->order_by;
		$order = $this->order;

		$positions = PositionModel::orderBy($order_by, $order)->get();

		if (count($positions) > 0) {
			foreach ($positions as $position) {
				// Total of winners
				$elected_count = $position['elected_count'];

				$candidates = CandidatesModel::leftJoin('persons as P', 'candidates.person_id', '=', 'P.id')->select('candidates.*','candidates.id as candidate_id', 'P.*')->where('position_id', $position['id'])->get();
				if (count($candidates) > 0) {
					foreach ($candidates as $candidate) {
						$votes_found = VotesModel::where('person_id', $candidate['person_id'])->where('position_id', $candidate['position_id'])->get();
						$candidate['total_votes'] = count($votes_found);
						CandidatesModel::where('id', $candidate['candidate_id'])->update(['total_votes' => $candidate['total_votes']]);
					}
				}

				// With Total Votes
				$candidates = CandidatesModel::leftJoin('persons as P', 'candidates.person_id', '=', 'P.id')->select('candidates.*','candidates.id as candidate_id','P.*')->where('position_id', $position['id'])->orderBy('total_votes', 'DESC')->get();

				if (count($candidates) > 0) {
					foreach ($candidates as $candidate) {
						if ($elected_count !== 0) {
							$elected_count -= 1;
							CandidatesModel::where('id', $candidate['candidate_id'])->update(['status' => 'Win']);
						}else{
							CandidatesModel::where('id', $candidate['candidate_id'])->update(['status' => 'Lose']);
						}
					}
				}

				// // For tiers
				// $candidates_had_same_votes = CandidatesModel::groupBy('id')->groupBy('total_votes')->where('position_id', $position['id'])->get();

				// if (count($candidates_had_same_votes) > 0){
				// 	for ($i = 0; $i < count($candidates_had_same_votes); $i++){
				// 		CandidatesModel::where('id', $candidates_had_same_votes[$i]['id'])->update(['status' => 'Tie']);
				// 	}
				// }


				$candidates = CandidatesModel::leftJoin('persons as P', 'candidates.person_id', '=', 'P.id')->select('candidates.*','candidates.id as candidate_id','P.*')->where('position_id', $position['id'])->orderBy('total_votes', 'DESC')->get();

				$position['candidates'] = $candidates;
				$position['total_candidates'] = count($candidates);
				$position['formatted_date'] = date('M d, Y h:i:s', strtotime($position['created_at']));
			}
		}
		return Response()->json(['type' => $this->type, 'data' => ['positions' => $positions]]);

	}



}