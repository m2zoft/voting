<?php 
namespace Bizwex\Voting\Handlers;

interface ViewingInterface{
	public function generate($params);
}