<?php

use Illuminate\Database\Seeder;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user_role = Sentinel::getRoleRepository()->createModel();
        $user_role->create([
		    'name' => 'System Administrator',
		    'slug' => 'systemadmin',
		]);
		$user_role->create([
		    'name' => 'System Administrator',
		    'slug' => 'systemadmin',
		]);
    }
}
