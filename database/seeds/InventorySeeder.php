<?php

use Illuminate\Database\Seeder;

class InventorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Bizwex\Inventory\Models\CompanyModel::class, 5)->create();
        factory(Bizwex\Inventory\Models\CompanyBrandModel::class, 5)->create();
    }
}
