<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/


// Company Seed
$factory->define(Bizwex\Inventory\Models\CompanyModel::class, function (Faker\Generator $faker) {

    return [
        'name' => $faker->company,
        'abbreviation' => $faker->tld,
        'owner_firstname' => $faker->firstName,
        'owner_middlename' => $faker->firstName,
        'owner_lastname'=> $faker->lastName,
    ];
});

// Company Brand Seed
$factory->define(Bizwex\Inventory\Models\CompanyBrandModel::class, function (Faker\Generator $faker) {

    return [
        'company_id' => $faker->randomDigit,
        'header_logo' => $faker->image,
        'report_logo' => $faker->image,
    ];
});